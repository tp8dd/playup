# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import time, sys

from django.db import models
from .utils import *
import copy

from splinter import Browser
from splinter.exceptions import ElementDoesNotExist



class Bot(models.Model):

	name = models.CharField(max_length=100, help_text='A human readable bot name')


	class Meta:
		verbose_name_plural = 'Bots'


	def __unicode__(self):
		return self.name

	def duplicate(self):
		kwargs = {}
		for field in self._meta.fields:
			kwargs[field.name] = getattr(self, field.name)
			# or self.__dict__[field.name]
		kwargs.pop('id')
		new_instance = self.__class__(**kwargs)
		new_instance.save()
		# now you have id for the new instance so you can
		# create related models in similar fashion
		fkeys_qs = self.step_set.all()
		new_fkeys = []
		for fkey in fkeys_qs:
			fkey_kwargs = {}
			for field in fkey._meta.fields:
				fkey_kwargs[field.name] = getattr(fkey, field.name)
			fkey_kwargs.pop('id')
			print fkey_kwargs
			print new_instance
			new_fkeys.append(fkeys_qs.model(**fkey_kwargs))
			new_instance.step_set.add(fkey)
		fkeys_qs.model.objects.bulk_create(new_fkeys)
		return new_instance



class Step(models.Model):

	class type():
		click = 'click'
		fill = 'fill'
		visit = 'visit'
		right_click = 'right_click'
		mouse_over = 'mouse_over'
		mouse_out = 'mouse_out'
		double_click = 'double_click'
		drag_and_drop = 'drag_and_drop'
		window = 'window'
		save = 'save'


	TYPE_CHOICES = (
		('click', type.click),
		('fill', type.fill),
		('visit', type.visit),
		('right_click', type.right_click),
		('mouse_over', type.mouse_over),
		('mouse_out', type.mouse_out),
		('double_click', type.double_click),
		('drag_and_drop', type.drag_and_drop),
		('window', type.window),
		('save', type.save)
	)


	def execute(self, thread, browser):
		try:
			print('Running step {} of {}'.format(self.pk, self.bot.step_set.all().count()))
			if self.method and self.method == 'sleep':
				time.sleep(float(self.value))
			elif self.type == 'window':
				browser.windows.current = browser.windows[int(self.value)]
			elif self.type == 'save':
				print 'saving........{}'.format(thread.email)
				email = Email.objects.filter(email=thread.email).first()
				print email
				email.processed = True
				email.save()
				print email.processed
			else:
				if self.method:
					elements = getattr(browser, self.method)(self.element)
					element = elements.first
					for element in elements:
						if element.visible:
							element = element
							break;
					if self.element == 'email':
						self.value = thread.email
					elif self.element == 'name':
						self.value = thread.email.split('@')[0]
					if not self.value:
						getattr(element, self.type)()
					else:
						getattr(element, self.type)(self.value)
				elif self.value:
					getattr(browser, self.type)(self.value)
				else:
					pass
		except Exception as e:
			print(e)



	class method():
		find_by_css = 'find_by_css'
		find_by_xpath = 'find_by_xpath'
		find_by_tag = 'find_by_tag'
		find_by_name = 'find_by_name'
		find_by_text = 'find_by_text'
		find_by_id = 'find_by_id'
		find_by_value = 'find_by_value'
		find_by_css = 'find_by_css'
		sleep = 'sleep'





	METHOD_CHOICES = (
		('find_by_css', method.find_by_css),
		('find_by_xpath', method.find_by_xpath),
		('find_by_tag', method.find_by_tag),
		('find_by_name', method.find_by_name),
		('find_by_text', method.find_by_text),
		('find_by_id', method.find_by_id),
		('find_by_value', method.find_by_value),
		('find_by_css', method.find_by_css),
		('sleep', method.sleep),
	)


	type = models.CharField(max_length=40, help_text='Type of action to perform', choices=TYPE_CHOICES, null=True, blank=True)
	bot = models.ForeignKey(to=Bot)
	element = models.CharField(max_length=2000, null=True, blank=True)
	method = models.CharField(choices=METHOD_CHOICES, max_length=20, null=True, blank=True)
	value = models.CharField(max_length=2000, null=True, blank=True)
	order = models.PositiveIntegerField(default=0, blank=False, null=False)

	class Meta(object):
		ordering = ['order']



class Proxy(models.Model):

	ip = models.GenericIPAddressField()
	port = models.PositiveSmallIntegerField()
	name = models.CharField(max_length=200)


	class Meta:
		verbose_name_plural = 'Proxies'

	def __unicode__(self):
		return '{} - {}'.format(self.name, self.ip)


class Email(models.Model):
	
	email = models.EmailField()
	username = models.CharField(max_length=200, null=True, blank=True)
	processed = models.BooleanField(default=False)
	date_joined = models.DateTimeField(auto_now=True)
	password = models.CharField(max_length=200, default='Cl1ckH3r3')



	def __unicode__(self):
		return self.email

