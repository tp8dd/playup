# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.core.management import call_command


from adminsortable2.admin import SortableInlineAdminMixin

from .models import Bot, Step, Proxy, Email



from import_export import resources
from import_export.admin import ImportExportModelAdmin



class EmailResource(resources.ModelResource):

    class Meta:
        model = Email
        import_id_fields = ('email',)
        exclude = ['id',]

def mark_processed(modeladmin, request, queryset):
	queryset.update(processed = True)

	
class EmailAdmin(ImportExportModelAdmin):
	resource_class = EmailResource
	list_filter = ['processed', 'date_joined']
	list_display = ['email', 'processed']
	search_fields = ['email']
	actions = [mark_processed]


class StepInline(SortableInlineAdminMixin, admin.TabularInline):
	model = Step


def send_bots(modeladmin, request, queryset):
	items = [item.pk for item in queryset.all()]
	items = ','.join(map(str, items))
	call_command('run', bot=items)



def duplicate(modeladmin, request, queryset):
	for item in queryset.all():
		item.duplicate()

class BotAdmin(admin.ModelAdmin):
	model = Bot
	actions = [send_bots, duplicate]
	inlines = [StepInline]


send_bots.short_description = "Trigger selected Bots"
duplicate.short_description = 'Duplicate this bot'
mark_processed.short_description = 'Mark this emails as processed'




admin.site.register(Bot, BotAdmin)
admin.site.register(Proxy)
admin.site.register(Email, EmailAdmin)