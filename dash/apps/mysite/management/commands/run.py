import urlparse, threading, time

from django.core.management.base import BaseCommand

from splinter import Browser
from pyvirtualdisplay import Display

from ...models import Bot

from ...utils import get_email


MAX = 550

executable_path = {'executable_path':'/usr/local/bin/chromedriver'}

threadLimiter = threading.BoundedSemaphore(MAX)

class Command(BaseCommand):
    help = 'Utility command to run the traffic bot'

    def add_arguments(self, parser):
        parser.add_argument('-b', '--bot', help='bots PK to trigger', type=str)
        parser.add_argument('-t', '--threads', help='Number of threads to run', type=int)
        parser.add_argument('-br', '--browser', help='Browser to use', type=str)
        parser.add_argument('--fullscreen', type=str, default='y', help='Browser fullscreen?')
        parser.add_argument('--virtual', type=str, default='n', help='Start virtual display?')


    def handle(self, *args, **options):
        threads = []


        fullscreen = True if options['fullscreen'] == 'y' else False

        print fullscreen

        threads_count = int(options['threads'])

        if 'y' == options['virtual']:
            print('Starting virtual display')
            display = Display(visible=0, size=(1440, 797)).start()


        bot_pks = options['bot']

        browser = options['browser']

        bot_pks = map(int, bot_pks.split(','))

        bots = Bot.objects.filter(pk__in = bot_pks)

        _start = time.time()

        print("Starting at {}".format(_start))

        for idx,bot in enumerate(bots):
            for i in range(0, threads_count):
                t = myThread(bot, browser, fullscreen)
                threads.append(t)


        for thread in threads:
             thread.start()

        for thread in threads:
             thread.join()

        print("execution time: %s seconds" % (time.time() - _start))
        if options['virtual'] == 'y':
            display.sendstop()



class myThread(threading.Thread):
        
        def __init__(self, bot, browser, fullscreen):
                threading.Thread.__init__(self)
                self.bot = bot
                self.email = get_email()
                self.browser = browser
                self.fullscreen = fullscreen


        def run(self):
                threadLimiter.acquire()
                try:
                    self.execute()
                finally:
                    threadLimiter.release()


        def execute(self):
            with Browser(self.browser, headless=False, fullscreen=self.fullscreen, **executable_path) as browser:
                steps = self.bot.step_set.all()
                print('Running bot {}'.format(self.bot.name))
                for step in steps:
                    step.execute(self, browser)
                browser.quit()
           