import urlparse

from django.core.management.base import BaseCommand


from ...models import Bot

class Command(BaseCommand):
    help = 'Utility command to run the traffic bot'

    def add_arguments(self, parser):
        parser.add_argument('-b', '--bot', help='bots PK to duplicate', type=int)


    def handle(self, *args, **options):
        bot = int(options['bot'])
        bot = Bot.objects.get(pk=bot)
        bot.duplicate()