import string, time, random
from faker import Faker
f = Faker()


def get_emails():
	username = f.email()
	emails = f.email()


def get_email():
		from .models import Email
		e = Email.objects.filter(processed=False).order_by('?').first()
		if e:
			return e.email
		return f.email()
