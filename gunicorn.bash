#!/bin/bash

NAME="playup"                                  # Name of the application
DJANGODIR=/Users/shashi/PycharmProjects/playup2             # Django project directory
SOCKFILE=/Users/shashi/PycharmProjects/playup2/run/gunicorn.sock  # we will communicte using this unix socket
USER=root                                        # the user to run as
NUM_WORKERS=3                                     # how many worker processes should Gunicorn spaw
DJANGO_SETTINGS_MODULE=dash.settings             # which settings file should Django use
DJANGO_WSGI_MODULE=dash.wsgi                     # WSGI module name
TIMEOUT=180
echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
source /Users/shashi/.virtualenvs/playup/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
/Users/shashi/.virtualenvs/playup/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --worker-connections 2000 \
  --worker-class gevent \
  --user=$USER \
 --timeout $TIMEOUT \
  --log-level=debug \
  --bind=0.0.0.0:8080
#  --bind=unix:$SOCKFILE
